# BP-Calendar-Sync
This tool is a must-have for all BP-Mediawork employees. This node-script synchronize all of your shifts of http://www.onquantm.com directly to your local OS X Calendar. So you do not have to login to the website, 
navigate to the calendar site to look up for your next shift. Thanks of iCloud - you can let synchronize your
shift entries over all of your devices. I never miss one again ;)

## Installation Nodes
This script uses [Phantomjs](http://phantomjs.org/) (standalone package, not a node module). So please install it first and test if it is avaiable globally. There is a known bug on OS X 10.10+ which brokes the Phantomjs module. Follow this link for a workaround: [Workaround by eugene1g](https://github.com/eugene1g/phantomjs/releases). Copy the phantomjs executable to /bin/phantomjs.
After that, cd into the downloaded bp-sync-folder and run:
`npm install`

In the last step, just run the main script like that:  
    node calendar-sync.js [options]

## Options  
There are different parameters you can use.

 - **m** - the month to sync, default ist the current month
	 - **m=+[1-12]** - set the month to (current month) + number
	 - **m=-[1 to 12]** - set the month to (current month) - number
 - **y** - the year to sync, default ist the current year  
	 - **y=+[1 to ...]** - set the year to (current year) + number
	 - **y=-[1 to ..]** - set the year to (current year) - number
 - **mode** - the mode of synchronisation, default: **ics**
	 - **mode=ics** - generates an ics file and a json file
	 - **mode=ical** - try to add events to you OS X Calendar app
 - **u** - your user name (e-mail)
	 - if not set the script asks you for your e-mail
 - **p** - your password
	 - if not set the script asks you for your password
 - **o** - the output path of the ics calendar file if its set, default is the current script folder

Run the script with options:
`node calendar-sync.js [m=MONTH] [y=YEAR] [mode=(ics|ical)]`
> node calendar-sync.js mode=ics
