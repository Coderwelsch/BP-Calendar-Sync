/*

	AUTHOR & DESCRIPTION

	Joseph Ribbe,
	Coderwelsch - Coding & Design,
	http://www.coderwelsch.com

	Version: 1.0
	-------------------------------------------------

	[COMPATIBILITY]
	OS X only

	[DESC]
	Grundfunktionalität besteht, kann natürlich noch
	vom CLI besser gestyled sein. In dieser Version
	werden bereits vorhandene Kalender Events gelöscht
	und ersetzt. Legt euch am Besten einen eigenen
	Kalender in der Kalender App von OS X an. Wenn Ihr
	keine Lust auf Eintippen der Login Daten habt dann
	schreibt sie in die unteren Variablen.

	Über den Terminal Aufruf könnt Ihr den zu
	synchronisierenden Monat und das Jahr und andere
	Optionen festlegen, z.B. so:
		node calendar.js m=5 y=2015 mode=ical o=/Pfad/zur/Ausgabe/Datei/ u=deine@email.de p=DEIN_PASSWORT

	Standardmäßig wird der derzeitige Monat abgefragt.
*/

// login data, asks for login data if not set
var username 	= 	"",
	password 	= 	"",
	calName		=	"Arbeit"; // name of used ical


// requirements
var Phantom 	= 	require( 'phantom' ),
	FileSystem	=	require( 'fs' ),
	Osascript 	= 	require( 'node-osascript' ),
	exportDir	=	__dirname + '/', // current script dir, change to your prefered path or use -o "/path/to/file/"
	calJsonName =	'calendar-data.json',
	icsName		=	'bp-mediawork-events.ics',
	Prompt		=	require( 'prompt' ),
	iCalToolkit =	require( 'ical-toolkit' ),
	timezone 	= 	'Europe/Berlin';


// global vars
var calendar 	= 	iCalToolkit.createIcsFileBuilder(), // ical object
	currentURL 	= 	'',
	args 		=	process.argv,
	readystate 	= 	null, // state of current website,
	calData 	=	{},

	syncLocale 	=	true,
	syncViaiCal =	false,

	calMonth 	=	undefined, // set later
	calYear 	=	undefined, // set later

	_phantom	= 	null, // phantom wrapper instance
	_page		=	null,

	loginPrompt = 	[], // set later

	calPattern	=	'http://onquantm.com/calendar/calendar/viewMonth?month={MONTH}&year={YEAR}',

	rmPattern	=	'tell application "Calendar" \r\ntell calendar "{CALENDAR_NAME}" \r\nset theEvent to first event whose summary contains "[{EVENT_ID}]" \r\nif exists theEvent then \r\ndelete theEvent \r\nreturn 1 \r\nend if \r\nreturn 0 \r\nend tell \r\nend tell \r\n',
	getPattern 	=	'tell application "Calendar" \r\ntell calendar "{CALENDAR_NAME}" \r\nset theEvent to first event whose summary contains "[{EVENT_ID}]" \r\nif exists theEvent then \r\nreturn 1 \r\nend if \r\nreturn 0 \r\nend tell \r\nend tell',
	setPattern	=	'tell application "Calendar" to make new event at end of calendar "{CALENDAR_NAME}" with properties {start date:date ("{START_DATE}" as string), end date:date ("{END_DATE}" as string), summary:\"{SUMMARY} [{EVENT_ID}]\"}',

	jqueryURL	=	'http://code.jquery.com/jquery-2.1.4.min.js';


// your functions
function logSummary () {
	var data = {
		events: 0,
		time: 0,
		shifts: {}
	};

	for ( var key in calData ) {
		if ( calData.hasOwnProperty( key ) ) {
			var item = calData[ key ],
				time = ( new Date('1-1-2000, ' + item.time.end ) - new Date('1-1-2000, ' + item.time.start ) ) / 3600000; // TODO

			data.events += 1;
			data.time += time;

			if ( data.shifts[ item.summary ] ) {
				data.shifts[ item.summary ] += 1;
			} else {
				data.shifts[ item.summary ] = 1;
			}
		}
	}

	console.log( 'Summary' );
	console.log( '    Shifts: ' + data.events );
	console.log( '    Work Time: ' + data.time );

	for ( var key in data.shifts ) {
		if ( data.shifts.hasOwnProperty( key ) ) {
			var item = data.shifts[ key ];

			console.log( '        ' + key + ': ' + item );
		}
	}
}

function dataSynced () {
	logSummary();

	console.log('Calendar Tool Finished Succesfully');

	process.exit( 1 );
}

// prototype functions
Object.prototype.clone = function ( obj ) {
	var newObj = {};

	obj = obj || this;

	for ( var key in obj ) {
		if ( obj.hasOwnProperty( key ) ) {
			newObj[ key ] = obj[ key ];
		}
	}

	return newObj;
};

// main functions
function removeCurrentMonthShifts ( data ) {
	for ( var id in data ) {
		if ( data.hasOwnProperty( id ) ) {
			var timeSplitted = String( data[ id ].date ).split( '-' ),
				modifiedTimeString = timeSplitted[ 1 ] + '-' + timeSplitted[ 0 ] + '-' + timeSplitted[ 2 ],
				dataTime = new Date( modifiedTimeString ),
				flagTime = null,
				maxTime = null;

			// TODO: have to fix that somewhere else - but not here ;)
			calMonth = calMonth || ( new Date() ).getMonth() + 1;
			calYear = calYear || ( new Date() ).getFullYear();

			flagTime = new Date( calMonth + '-01-' + calYear );
			maxTime = new Date( ( calMonth + 1 ) + '-01-' + calYear );

			if ( dataTime >= flagTime && dataTime < maxTime ) {
				delete data[ id ];
			}
		}
	}

	return data;
}

function syncDataWithJSONFile () {
	var events = [],
		loadedCalData = {},
		mergedData = {},
		newData = {};

	FileSystem.readFile( exportDir + calJsonName, function ( error, data ) {
		var length = 0;

		if ( data ) { // if local json file exists
			loadedCalData = JSON.parse( data );
			mergedData = loadedCalData.clone();
			mergedData = removeCurrentMonthShifts( mergedData );

			// convert website cal data array to object
			for ( var i = 0; i < calData.length; i++) {
				var item = calData[ i ];
				newData[ item.id ] = item;
			}

			// sync local events with website events
			for ( var newKey in newData ) {
				if ( newData.hasOwnProperty( newKey ) ) {
					mergedData[ newKey ] = newData[ newKey ];
				}
			}

		} else { // if local json file does not exists
			// convert website cal data array to object
			for ( var i = 0; i < calData.length; i++ ) {
				var item = calData[ i ];

				mergedData[ item.id ] = item;
			}
		}

		saveJsonData();
	} );

	function saveJsonData () {
		FileSystem.writeFile( exportDir + calJsonName, JSON.stringify( mergedData, null, 4 ), { flag: 'w+' }, function ( error ) {
			if ( error ) {
				throw error;
			}

			createICSFile();
		} );
	}

	function createICSFile () {
		var icsCalExportString = '';
		calendar.spacers = true; // use spaces
		calendar.NEWLINE_CHAR = '\r\n';
		calendar.throwError = false;
		calendar.ignoreTZIDMismatch = true;

		calendar.calname = calName;
		calendar.timezone = timezone;
		calendar.tzid = timezone;
		calendar.method = 'PUBLISH';

		for ( var key in mergedData ) {
			if ( mergedData.hasOwnProperty( key ) ) {
				var item = mergedData[ key ],
					dateComps = item.date.split( '-' ), // convert date to correct format
					day = dateComps[ 0 ],
					month = dateComps[ 1 ],
					year = dateComps[ 2 ],
					realDate = month + '-' + day + '-' + year,

					event = { // main event
						start: new Date( realDate + ' ' + item.time.start ),
						end: new Date( realDate + ' ' + item.time.end ),
						summary: item.summary + ' [' + item.id + ']',
						transp: 'OPAQUE',
						organizer: {
							name: 'BP Mediawork',
							email: "info@bpmediawork.de"
						},
						url: 'http://www.onquantm.com'
					};

				events.push( event );
			}
		}

		calendar.events = events;

		icsCalExportString = calendar.toString();

		if ( icsCalExportString instanceof Error ) {
			throw( 'There was an error creating the ics events: ' + icsCalExportString );
		} else {
			FileSystem.writeFile( exportDir + icsName, icsCalExportString, { flag: 'w+' }, function ( error ) {
				if ( error ) {
					console.log( "Error: The file " + icsName + " is in use and can't be overwritten. Please close your calendar app and try it again!" );

					throw error;
				}

				dataSynced();
			} );
		}
	}
}

function syncDataWithCalendarApp () {
	function insert ( timeDataIndex ) {
		var item = calData[ timeDataIndex ],
			set  = setPattern
						.replace( '{CALENDAR_NAME}', calName )
						.replace( '{EVENT_ID}', item.id )
						.replace( '{START_DATE}', item.date + ' ' + item.time.start )
						.replace( '{END_DATE}', item.date + ' ' + item.time.end )
						.replace( '{SUMMARY}', item.summary )
						.replace( '{EVENT_ID}', item.id );

		execScript( set, function ( error, result, raw ) {
			if ( timeDataIndex < calData.length - 1 ) {
				var item = calData[ timeDataIndex ];

				console.log( '+ Event "{EVENT}" at {DATE}'.replace( '{EVENT}', item.summary ).replace( '{DATE}', item.date ) );

				check( timeDataIndex + 1 );
			} else {
				dataSynced();
			}
		} );
	}

	function remove ( timeDataIndex ) {
		var item = calData[ timeDataIndex ],
			rm   = rmPattern
						.replace( '{CALENDAR_NAME}', calName )
						.replace( '{EVENT_ID}', item.id );

		execScript( rm, function ( error, result, raw ) {
			// console.log( '- Event "{EVENT}" at {DATE}'.replace( '{EVENT}', item.summary ).replace( '{DATE}', item.date ) );

			insert( timeDataIndex );
		} );
	}

	function check ( timeDataIndex ) {
		var item = calData[ timeDataIndex ],
			get  = getPattern
						.replace( '{CALENDAR_NAME}', calName )
						.replace( '{EVENT_ID}', item.id );

		execScript( get, function ( error, result, raw ) {
			if ( parseInt( result ) === 1 || typeof result === 'undefined' ) {
				remove( timeDataIndex );
			} else {
				throw( 'Unknown return value of applescript "' + get + '": ' + result );
			}
		} );
	}

	function execScript ( script, callback ) {
		Osascript.execute( script, callback );
	}

	check( 0 );
}

function processArguments () {
	for ( var i = 2; i < process.argv.length; i++ ) {
		var item = process.argv[ i ],
			flagKey = item.split( '=' )[ 0 ],
			flagValue = item.split( '=' )[ 1 ];

		if (flagKey === 'node' ) {
			continue;
		}

		switch ( flagKey ) {
			// month flag
			case 'm':
				if ( flagValue.indexOf( '+' ) === 0 ) {
					var currentMonth = (new Date()).getMonth() + 1;

					addMonths = parseInt( flagValue.replace( '+', '' ) );

					calMonth = currentMonth + addMonths;
				} else if ( flagValue.indexOf( '-' ) === 0 ) {
					var currentMonth = (new Date()).getMonth() + 1;

					divMonths = parseInt( flagValue.replace( '-', '' ) );

					calMonth = currentMonth - divMonths;
				} else {
					calMonth = parseInt( flagValue );
				}

				if ( calMonth < 1 ) {
					console.log( 'Your requested month is out of the range: ' + calMonth + '. It was set to the first month.');

					calMonth = 0;
				}

				if ( calMonth > 12 ) {
					console.log( 'Your requested month is out of the range: ' + calMonth + '. It was set to the last month.');

					calMonth = 12;
				}

				console.log( "Set month to " + calMonth );
			break;

			// year flag
			case 'y':
				calYear = flagValue;
				console.log( "Set year to " + calYear );
			break;

			// mode flag
			case 'mode':
				if ( flagValue !== 'ics' ) {
					syncLocale = true;
					syncViaiCal = false;

					if ( process.platform !== 'darwin' ) {
						console.log( 'Sorry, but the iCal sync only works for OS X users. But you can export the calendar to an .ics file with -mode=ics' );
						process.exit( 0 );
					}
				} else if ( flagValue === 'ics' ) {
					syncLocale = false;
					syncViaiCal = true;
				} else if ( falgValue === 'gCal' ) {

				} else {
					console.log( 'Set unknown calendar mode: ' + flagValue + '. Use default mode instead.' );
				}
			break;

			case 'u':
				username = flagValue;
			break;

			case 'p':
				password = flagValue;
			break;

			// ics output path
			case 'o':
				exportDir = flagValue;
			break;

			default:
				console.log( 'Unknown flag found: ' + flagKey );
			break;
		}
	}
}

function loadCalendarData () {
	var date 	= 	new Date(),
		month 	= 	calMonth || date.getMonth() + 1,
		year 	=	calYear || date.getFullYear(),
		url 	=	calPattern.replace( '{MONTH}', month ).replace( '{YEAR}', year );

	_page.open( url, function ( status ) {
		if ( status === 'success' ) {
			console.log( 'Load Calendar Data...' );

			_page.includeJs( jqueryURL, function () {
				_page.evaluate( function () {
					var timeData = [];

					$( 'table.calendar.calendar-month' ).each( function () {
						var $this = $( this ),
							$body = $this.find( 'tbody' ),
							$head = $this.find( 'thead' );

						$body.find( 'tr td.day:not(.outOfMonth)' ).each( function () {
							if ( $( this ).find( '> div' ).length ) {
								var index = $( this ).index(),
									date  = $head.find( 'tr td' ).eq( index ).html().replace( ' ', '' ).split( ',' )[ 1 ].split( '.' ),
									time  = $( this ).find( '.time' ).text().split( '-' ),
									year  = ( new Date() ).getFullYear(),
									data  = {
										date: 	date[ 0 ] + '-' + date[ 1 ] + '-' + year,
										id: 	$( this ).find( 'div[data-aid]' ).attr( 'data-aid' ),
										time: {
											start: 	time[ 0 ] + ':00',
											end: 	time[ 1 ] + ':00'
										},
										summary:$( this ).find( 'span:contains("BILD")' ).text()
									};

								timeData.push( data );
							}
						} );
					} );

					return JSON.stringify( timeData, null, 4 );
				}, function ( result ) {
					// remove umlaut problems
					result = result.replace( /Ã¤/g, 'ä' );

					calData = JSON.parse( result );


					if ( syncLocale ) {
						syncDataWithCalendarApp();
					} else {
						syncDataWithJSONFile();
					}
				} );
			} );
		}
	} );
}

function loginToOnquantm () {
	function load () {
		console.log( 'Logging In To Onquantm...' );

		_page.open( 'http://onquantm.com/site/login?', 'post', 'LoginForm[email]=' + username + '&LoginForm[password]=' + password, function ( status ) {
			if ( status === 'success' ) {
				loadCalendarData();
			}
		} );
	}

	if ( username === '' ) {
		loginPrompt.push( { name: 'username' } );
	}

	if ( password === '' ) {
		loginPrompt.push( { name: 'password', hidden: true } );
	}

	if ( calName === '' ) {
		loginPrompt.push( { name: 'calendar-name' } );
	}

	if ( loginPrompt === [] ) {
		load();
	} else {
		Prompt.start();
		Prompt.get( loginPrompt, function ( err, result ) {
		    if ( err ) {
		    	return onErr( err );
		    }

		    username = result.username || username;
		    password = result.password || password;
		    calName  = result[ 'calendar-name' ] || calName;

	    	load();
		} );
	}
}

function initComplete () {
	loginToOnquantm();
}

function onURLChanged () {

}


// main functions
function updateReadyStateOfPage ( callback_ ) {
	_page.evaluate( function () {
		return document.readyState;
	}, function ( result_ ) {
		if ( callback_ !== undefined ) {
			callback_( result_ );
		}
	} );
}

function fnToString ( fn_, args_ ) {
	var s = fn_.toString();

	for ( var key in args_ ) {
		var v = args_[ key ];

		switch ( typeof v ) {
			case 'string':
				v = '"' + v + '"';
			break;

			case 'number':
				// do nothing
			break;

			case 'object':
				v = JSON.stringify( v );
			break;

			default:
				throw( 'Cant find type of variable' );
			break;
		}

		s = s.replace( key, v );
	}

	return s;
}

function pageReady () {
	setInterval( function () {
		var url = '';

		_page.evaluate( function () {
			return window.location.href;
		}, function ( result_ ) {
			url = result_;

			if ( currentURL !== url && currentURL !== '') {
				currentURL =  url;

				onURLChanged();
			}
		} );
	}, 100 );

	initComplete();
}

function pageInitialized ( page_ ) {
	_page = page_;

	pageReady();
}

function phantomReady () {
	_phantom.createPage( pageInitialized );
}

function phantomInitialized ( phantom_ ) {
	_phantom = phantom_;

	phantomReady();
}

function createPhantomSession () {
	Phantom.create( phantomInitialized );
}

function init () {
	// fetch arguments
	processArguments();

	// start with phantom
	createPhantomSession();
}

init();
